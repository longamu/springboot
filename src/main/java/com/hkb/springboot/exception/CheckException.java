/**
 * Created on 2017年12月1日 上午10:01:56 <br>
 */
package com.hkb.springboot.exception;

/**
 * 自定义的校验异常 .<br>
 * 
 * @author hkb <br>
 */
public class CheckException extends RuntimeException {

    /**
     * 序列化id
     */
    private static final long serialVersionUID = 1L;

    public CheckException() {
        super();
    }

    public CheckException(String message) {
        super(message);
    }

    public CheckException(Throwable cause) {
        super(cause);
    }

    public CheckException(String message, Throwable cause) {
        super(message, cause);
    }

    public CheckException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
