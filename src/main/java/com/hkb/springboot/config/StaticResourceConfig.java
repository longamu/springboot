/**
 * Created on 2018年3月23日 上午11:38:28
 */
package com.hkb.springboot.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * 静态资源配置 . <br>
 * 
 * @author hkb <br>
 */
@Configuration
public class StaticResourceConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        // 自定义静态资源文件路径
        // http://127.0.0.1:8080/springboot/dir/imgs/t.png
        registry.addResourceHandler("/dir/**").addResourceLocations("classpath:/static/");
    }

}
