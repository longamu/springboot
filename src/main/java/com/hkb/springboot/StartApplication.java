/**
 * Created on 2018年3月11日 下午9:03:56
 */
package com.hkb.springboot;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * SpringBoot启动类 . <br>
 * 继承SpringBootServletInitializer类并重写configure方法 <br>
 * 打包成war包的形式 <br>
 * ServletComponentScan表示开启servlet的注解 <br>
 *
 * @author hkb <br>
 */
@SpringBootApplication
@ServletComponentScan
/*
 * 优化启动(暂未实现)-在下面的代码中我们可以删掉我们不需要的组件信息,比如在项目中没有使用Jmx和WebSocket功能的话,那么我们就可以删除<br>
 * JmxAutoConfiguration.class和WebSocketAutoConfiguration.class
 */
// @Configuration
// @Import({ DispatcherServletAutoConfiguration.class,
// EmbeddedServletContainerAutoConfiguration.class,
// ErrorMvcAutoConfiguration.class, HttpEncodingAutoConfiguration.class,
// HttpMessageConvertersAutoConfiguration.class,
// MultipartAutoConfiguration.class,
// ServerPropertiesAutoConfiguration.class,
// PropertyPlaceholderAutoConfiguration.class,
// ThymeleafAutoConfiguration.class, WebMvcAutoConfiguration.class, })
public class StartApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(StartApplication.class);
    }

    /**
     * 隐藏banner启动方式
     */
    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(StartApplication.class);
        // 设置banner的模式为隐藏
        springApplication.setBannerMode(Banner.Mode.CONSOLE);
        // 启动springboot应用程序
        springApplication.run(args);

        // 原启动方式
        // SpringApplication.run(StartApplication.class, args);
    }

    // /**
    // * 配合优化注解注入controller
    // *
    // * @return LogController
    // */
    // @Bean
    // public LogController logController() {
    // return new LogController();
    // }
    //
    // /**
    // * 配合优化注解注入controller
    // *
    // * @return
    // */
    // @Bean
    // public StudentController studentController(StudentService studentService)
    // {
    // return new StudentController(studentService);
    // }
    //
    // @Bean
    // public StudentService studentService(StudentDao studentDao) {
    // return new StudentServiceImpl(studentDao);
    // }
    //
    // /**
    // * 配合优化注解注入controller
    // *
    // * @return
    // */
    // @Bean
    // public UploadController uploadController() {
    // return new UploadController();
    // }

}
